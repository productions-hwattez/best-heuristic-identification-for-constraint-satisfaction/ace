package bandit.reward;

import solver.Statistics;

public class AUVRReward implements RewardStrategy {

    private Statistics stats;
    private long prevnFailed;
    private double prevCumulated;

    public AUVRReward(Statistics stats) {
        this.stats = stats;
    }

    @Override
    public double getReward() {
        long diffNfailed = stats.nFailedAssignments - this.prevnFailed;
        this.prevnFailed = stats.nFailedAssignments;

        double diffCum = stats.cumulatedDepthRatioAtEachFailed - this.prevCumulated;
        this.prevCumulated = stats.cumulatedDepthRatioAtEachFailed;

        return diffCum / diffNfailed;
    }

    @Override
    public void afterRun() {
    }

}


