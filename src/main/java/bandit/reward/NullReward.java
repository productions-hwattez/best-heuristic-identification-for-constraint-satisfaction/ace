package bandit.reward;

import solver.Statistics;

public class NullReward implements RewardStrategy {

    public NullReward(Statistics stats) {
    }

    @Override
    public double getReward() {
        return 0.;
    }

    @Override
    public void afterRun() {
    }

}


