package bandit.reward;

public interface RewardStrategy {
    double getReward();
    void afterRun();
}

