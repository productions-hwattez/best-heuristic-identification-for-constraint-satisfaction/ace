package bandit;

import solver.Solver;

public class EpsilonGreedy extends BanditPolicy {

    private static final double EPSILON = .1;

    public EpsilonGreedy(Solver solver, int k) {
        super(solver, k);
    }

    @Override
    public int nextAction() {
        if (this.totalCalls < this.k) {
            return this.totalCalls;
        } else if (rand.nextFloat() < EPSILON) {
            return this.rand.nextInt(this.k);
        }

        int maxValueIndex = -1;
        double valuesMax = -Double.MAX_VALUE;

        for (int i = 0; i < armMean.length; i++) {

            double val = this.armMean[i];

            if (valuesMax < val) {
                valuesMax = val;
                maxValueIndex = i;
            }
        }

        return maxValueIndex;
    }

    @Override
    protected void subUpdate(int i, double reward) {
        // Nothing to do
    }
}
