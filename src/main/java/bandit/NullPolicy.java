package bandit;

import solver.Solver;

public class NullPolicy extends BanditPolicy {

    public NullPolicy(Solver solver, int k) {
        super(solver, k);
    }

    @Override
    public int nextAction() {
        return -1;
    }

    @Override
    protected void subUpdate(int i, double reward) {

    }
}
