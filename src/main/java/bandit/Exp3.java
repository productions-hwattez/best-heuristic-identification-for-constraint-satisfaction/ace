package bandit;

import solver.Solver;

import java.util.Arrays;

public class Exp3 extends BanditPolicy {

    private static final double DOUBLE_EPSILON = 0.0001;

    private final double[] armSumEstimatedRewards;
    private final double[] armProbabilities;


    public Exp3(Solver solver, int k) {
        super(solver, k);
        armSumEstimatedRewards = new double[k];
        armProbabilities = new double[k];
        Arrays.fill(armProbabilities, 1. / k);
    }

    @Override
    public int nextAction() {
        if (this.totalCalls < this.k) {
            return this.totalCalls;
        }

        double randD = rand.nextDouble();

        for (int i=0; i < armProbabilities.length; i++) {
            randD -= armProbabilities[i];
            if (randD <= DOUBLE_EPSILON) {
                return i;
            }
        }
        return this.k - 1;
    }

    @Override
    protected void subUpdate(int i, double reward) {
        double estimatedReward = reward / Math.max(0.01, this.armProbabilities[i]);
        this.armSumEstimatedRewards[i] += estimatedReward;

        if (this.totalCalls < this.k) {
            return;
        }

        double stepSize = 1. / Math.sqrt(this.totalCalls);

        double z = 0;

        for (double sumR : this.armSumEstimatedRewards) {
            z += Math.exp(stepSize * sumR);
        }

        for (int j=0; j < this.armProbabilities.length; j++) {
            this.armProbabilities[j] = Math.exp(stepSize * this.armSumEstimatedRewards[j]) / z;
        }

    }
}
