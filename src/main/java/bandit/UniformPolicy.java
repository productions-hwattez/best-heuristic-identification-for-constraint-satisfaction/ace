package bandit;

import solver.Solver;

public class UniformPolicy extends BanditPolicy {

    public UniformPolicy(Solver solver, int k) {
        super(solver, k);
    }

    @Override
    public int nextAction() {
        return this.rand.nextInt(this.k);
    }

    @Override
    protected void subUpdate(int i, double reward) {

    }
}
