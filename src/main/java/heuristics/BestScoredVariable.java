package heuristics;

import problem.Problem;
import solver.Solver;
import variables.Variable;

import java.util.Random;

public class BestScoredVariable {
    private Variable variable;
    public double score;
    public boolean minimization;
    private boolean discardAux;

    // Necessary structures for Gomes equivalence
    private Variable[] vars;
    private double[] scores;
    private int varsSize = 0;
    private final double equi;
    private final Random rand;

    public BestScoredVariable(boolean discardAux, Solver solver) {
        this.discardAux = discardAux;

        // Necessary structures for Gomes equivalence
        this.vars = solver.head.control.varh.varEqui != 0.0 ? new Variable[solver.problem.variables.length] : null;
        this.scores = solver.head.control.varh.varEqui != 0.0 ? new double[solver.problem.variables.length] : null;
        this.equi = solver.head.control.varh.varEqui;
        this.rand = new Random(solver.head.control.general.seed);
    }

    public BestScoredVariable(Solver solver) {
        this(false, solver);
    }

    public BestScoredVariable reset(boolean minimization) {
        this.variable = null;
        this.score = minimization ? Double.MAX_VALUE : Double.NEGATIVE_INFINITY;
        this.minimization = minimization;
        this.varsSize = 0;
        return this;
    }

    public boolean update(Variable x, double s) {
        if (discardAux && x.isSolverAux()) {
            assert x.id().startsWith(Problem.AUXILIARY_VARIABLE_PREFIX);
            return false;
        }
        if (minimization) {
            if (s < score) {
                variable = x;
                score = s;
            }
        } else {
            if (s > score) {
                variable = x;
                score = s;
            }
        }

        if (this.isEqui(s)) {
            this.vars[this.varsSize] = x;
            this.scores[this.varsSize++] = s;
        }

        return this.variable == x; // not updated
    }

    private boolean isEqui(double s) {
        double best = Math.abs(score);
        best = best < 1e-10 ? 1 : best;
        //System.out.println(Math.abs(s - score) / best);
        return this.vars != null && Math.abs(s - score) / best < equi;
    }

    private Variable chooseEquiVarAtRandom() {
        if (this.vars == null) {
            return null;
        }

        int newVarsSize = 0;

        for (int i=0; i<this.varsSize; i++) {
            if (this.isEqui(this.scores[i])) {
                this.switchVars(i, newVarsSize++);
            }
        }

        this.varsSize = newVarsSize;

        return this.varsSize==0 ? null : this.vars[this.rand.nextInt(this.varsSize)];
    }

    private void switchVars(int i, int j) {
        Variable tmpVar = this.vars[i];
        double tmpScore = this.scores[i];

        this.vars[i] = this.vars[j];
        this.scores[i] = this.scores[j];

        this.vars[j] = tmpVar;
        this.scores[j] = tmpScore;
    }

    public Variable getVariable() {
        Variable v = chooseEquiVarAtRandom();
        return v != null ? v : this.variable;
    }
}
