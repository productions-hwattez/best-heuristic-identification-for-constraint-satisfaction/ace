package heuristics;

import constraints.Constraint;
import sets.SetDense;
import solver.Solver;
import variables.Variable;

import java.util.function.BiFunction;

public enum CACDVariant {
    CACD((c, v) -> 1. / (c.futvars.size() * (v.dom.size() == 0 ? 0.5 : v.dom.size()))),
    CA((c, v) -> 1. / c.futvars.size()),
    CD((c, v) -> 1. / v.dom.size() == 0 ? 0.5 : v.dom.size()),
    IA((c, v) -> 1. / c.scp.length),
    ID((c, v) -> 1. / v.dom.initSize());
    // Maybe in the future -> CA_ON_CD, CD_ON_CA, CACD_INV, ...

    private final BiFunction<Constraint, Variable, Double> scoreFunction;

    CACDVariant(BiFunction<Constraint, Variable, Double> scoreFunction) {
        this.scoreFunction = scoreFunction;
    }

    public double apply(Constraint c, Variable v) {
        return this.scoreFunction.apply(c, v);
    }
}
