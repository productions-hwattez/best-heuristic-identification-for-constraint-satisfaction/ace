package heuristics.composition;

import constraints.Constraint;
import solver.Solver;
import variables.Variable;

import java.util.Arrays;

public class HVCAtNode extends HeuristicVariableComposition {

    private final int[] varhByDepth;
    private final int[] maxDepth;

    public HVCAtNode(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);
        this.varhByDepth = new int[solver.problem.variables.length];
        this.maxDepth = new int[solver.problem.variables.length];
        Arrays.fill(this.varhByDepth, -1);
    }

    @Override
    public void internalBeforeRun() {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeRun();
        }

        this.selectNextVarH();
    }

    private void selectNextVarH() {
        this.lastAction = this.bandit.nextAction();
        this.currentVarHManager = varHManagers.get(this.lastAction);
        this.currentVarH = this.currentVarHManager.getVarH();
        this.currentVarHManager.select();
    }

    private void updateVarHAction(int depth) {
        this.bandit.update(this.varhByDepth[depth], (1. - (this.maxDepth[depth] - depth) / (this.solver.maxDepth + 1)));
        this.varhByDepth[depth] = -1;
    }

    @Override
    public void afterRun() {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterRun();
        }

        for (int i=0; i<this.varhByDepth.length; i++) {
            if (this.varhByDepth[i] == -1) {
                return;
            }
            this.updateVarHAction(this.varhByDepth[i]);
        }
    }

    @Override
    public void whenWipeout(Constraint c, Variable x) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().whenWipeout(c, x);
        }
    }

    @Override
    public void beforeAssignment(Variable x, int a) {
        int depth = this.solver.depth();
        this.varhByDepth[depth] = this.lastAction;
        this.maxDepth[depth] = depth;

        for (int i=0; i<depth; i++) {
            this.maxDepth[i] = Math.max(depth, this.maxDepth[i]);
        }

        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeAssignment(x, a);
        }
    }

    @Override
    public void afterAssignment(Variable x, int a) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterAssignment(x, a);
        }

        this.selectNextVarH();
    }

    @Override
    public void afterUnassignment(Variable x) {
        this.updateVarHAction(this.solver.depth());

        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterUnassignment(x);
        }
    }

    @Override
    public double scoreOf(Variable x) {
        return this.currentVarH.scoreOf(x);
    }

    @Override
    public Variable bestUnpriorityVar() {
        return this.currentVarH.bestUnpriorityVar();
    }

    @Override
    public String toString() {
        return this.bandit.toString();
    }

}
