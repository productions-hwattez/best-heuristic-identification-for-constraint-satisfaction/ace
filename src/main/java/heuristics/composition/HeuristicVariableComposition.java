package heuristics.composition;

import bandit.reward.RewardStrategy;
import heuristics.HeuristicVariables;
import heuristics.HeuristicVariablesDynamic;
import solver.Solver;
import utility.Reflector;
import bandit.BanditPolicy;

import java.util.ArrayList;
import java.util.List;

public abstract class HeuristicVariableComposition extends HeuristicVariablesDynamic {

    protected RewardStrategy reward;
    protected boolean firstRewardInstanciation = true;

    protected final List<VarHManager> varHManagers;
    protected HeuristicVariables currentVarH;
    protected VarHManager currentVarHManager;

    protected final BanditPolicy bandit;
    protected int lastAction;


    public HeuristicVariableComposition(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);
        this.varHManagers = this.buildVarHManagers(this.solver.head.control.varh.varhSet.split(","));
        this.bandit = Reflector.buildObject(solver.head.control.varh.bandit, BanditPolicy.class, this.solver, this.varHManagers.size());
        this.currentVarHManager = this.varHManagers.get(0);
        this.currentVarH = this.currentVarHManager.getVarH();
    }

    private List<VarHManager> buildVarHManagers(String[] hNames) {
        List<VarHManager> managers = new ArrayList<>(hNames.length);

        for (String hName : hNames) {
            managers.add(HeuristicBuilder.valueOf(hName).build(this.solver));
        }

        return managers;
    }

    private void rewardFunctionInstanciation() {
        if (!this.firstRewardInstanciation) {
            return;
        }

        this.firstRewardInstanciation = false;

        this.reward = Reflector.buildObject(solver.head.control.varh.reward, RewardStrategy.class, this.solver.stats);
    }

    @Override
    public void beforeRun() {
        this.rewardFunctionInstanciation();
        this.internalBeforeRun();
    }

    protected abstract void internalBeforeRun();

    @Override
    public String toString() {
        return this.currentVarHManager.toString();
    }

}
