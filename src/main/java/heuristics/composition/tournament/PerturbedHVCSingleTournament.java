package heuristics.composition.tournament;

import constraints.Constraint;
import heuristics.composition.HeuristicVariableComposition;
import heuristics.composition.VarHManager;
import solver.Solver;
import utility.Reflector;
import variables.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PerturbedHVCSingleTournament extends HeuristicVariableComposition {

    private final int mMatchs;
    // the championship
    private int currentIndex = 0;
    private final List<MDuels> champShip = new ArrayList<>();


    public PerturbedHVCSingleTournament(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);
        this.mMatchs = this.solver.head.control.restarts.nSubRuns;
        //this.rewardAgg = Reflector.buildObject(solver.head.control.varh.rewardAgg, CSRewardAggregation.class);
    }

    @Override
    public void internalBeforeRun() {
        this.selectNextVarH();

        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeRun();
        }
    }

    private void selectNextVarH() {
        int level = this.whichLevel();

        if (level == 0) {
            this.currentVarHManager = this.varHManagers.get((this.currentIndex++ / this.mMatchs) % this.varHManagers.size());
        } else {
            this.currentVarHManager = this.champShip.get(level - 1).getTheWinner();
        }

        this.currentVarH = this.currentVarHManager.getVarH();
        this.currentVarHManager.select();

        // In the case K is odd, the last one must be different from the first one of the new combination of varHManagers
        if (this.currentIndex % (this.varHManagers.size() * this.mMatchs) == 0) {
            VarHManager previousLastOne = this.varHManagers.size() % 2 == 1 ? this.varHManagers.get(this.varHManagers.size() - 1) : null;
            do {
                Collections.shuffle(this.varHManagers, this.solver.head.random);
            } while (previousLastOne == this.varHManagers.get(0));
        }
    }

    private int whichLevel() {
        //System.out.println("this.solver.restarter.offset=" + this.solver.restarter.offset);
        return Integer.numberOfTrailingZeros((int) (this.solver.restarter.offset / this.solver.restarter.baseCutoff));
    }

    @Override
    public void afterRun() {
        this.reward.afterRun();
        this.computeTheVersus();

        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterRun();
        }
    }

    private void computeTheVersus() {
        int level = this.whichLevel();

        if (level == this.champShip.size()) {
            int k = this.solver.head.control.restarts.decSubRuns ? level : 0;
            long decFactor = 1L << k;
            int m = Math.max((int) (this.solver.head.control.restarts.nSubRuns / decFactor), 1);
            //System.out.println("m=" + m);
            this.champShip.add(Reflector.buildObject(solver.head.control.varh.defMDuels, MDuels.class, m));
        }

        double r;

        r = this.reward.getReward();

        this.champShip.get(level).add(this.currentVarHManager, r);
    }


    @Override
    public void whenWipeout(Constraint c, Variable x) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().whenWipeout(c, x);
        }
    }

    @Override
    public void beforeAssignment(Variable x, int a) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeAssignment(x, a);
        }
    }

    @Override
    public void afterAssignment(Variable x, int a) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterAssignment(x, a);
        }
    }

    @Override
    public void afterUnassignment(Variable x) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterUnassignment(x);
        }
    }

    @Override
    public double scoreOf(Variable x) {
        return this.currentVarH.scoreOf(x);
    }

    @Override
    public Variable bestUnpriorityVar() {
        return this.currentVarH.bestUnpriorityVar();
    }

    @Override
    public String toString() {
        return this.currentVarHManager.toString();
    }

}
