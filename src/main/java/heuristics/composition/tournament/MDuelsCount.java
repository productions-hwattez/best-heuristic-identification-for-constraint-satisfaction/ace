package heuristics.composition.tournament;

import heuristics.composition.VarHManager;

import java.util.Random;

public class MDuelsCount extends MDuels {

    private final Random rand = new Random(1);

    public MDuelsCount(int mMatchs) {
        super(mMatchs);
    }

    @Override
    protected VarHManager getTheBestCandidate() {
        int counter = 0;

        for (int i=0; i<this.mMatchs; i++) {
            counter += this.matchs[i] > this.matchs[i+this.mMatchs] ? -1 : 1;
        }

        if (counter == 0) {
            this.candidates.get(this.rand.nextInt(2));
        }

        return counter < 0 ? this.candidates.get(0) : this.candidates.get(1);
    }
}
