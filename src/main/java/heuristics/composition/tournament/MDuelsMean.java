package heuristics.composition.tournament;

import heuristics.composition.VarHManager;

public class MDuelsMean extends MDuels {

    public MDuelsMean(int mMatchs) {
        super(mMatchs);
    }

    @Override
    protected VarHManager getTheBestCandidate() {
        double r1 = 0, r2 = 0;

        for (int i=0; i<this.mMatchs; i++) {
            r1 += this.matchs[i];
            r2 += this.matchs[i+this.mMatchs];
        }

        return r1 > r2 ? this.candidates.get(0) : this.candidates.get(1);
    }
}
