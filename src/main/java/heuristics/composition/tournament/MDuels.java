package heuristics.composition.tournament;

import heuristics.composition.VarHManager;

import java.util.ArrayList;
import java.util.List;

public abstract class MDuels {
    protected final int mMatchs;
    protected final double[] matchs;
    protected int current;
    protected List<VarHManager> candidates = new ArrayList<>(2);
    protected VarHManager winner;

    public MDuels(int mMatchs) {
        this.mMatchs = mMatchs;
        this.matchs = new double[mMatchs<<1];
    }

    public void add(VarHManager candidate, double reward) {
        if (current % mMatchs == 0) {
            candidates.add(candidate);
            this.winner = null;
        }
        this.matchs[current++] = reward;
    }

    public VarHManager getTheWinner() {
        if (this.winner != null) {
            return this.winner;
        }
        if (candidates.size() != 2) {
            throw new RuntimeException("Number of candidates must be equal to two.");
        }
        if (current != mMatchs<<1) {
            throw new RuntimeException("Number of matchs does not correspond.");
        }

        this.winner = this.getTheBestCandidate();
        this.reinit();

        return this.winner;
    }

    protected abstract VarHManager getTheBestCandidate();

    private void reinit() {
        this.current = 0;
        this.candidates.clear();
    }
}

