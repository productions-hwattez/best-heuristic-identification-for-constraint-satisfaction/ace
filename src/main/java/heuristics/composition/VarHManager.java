package heuristics.composition;

import heuristics.HeuristicVariables;
import solver.Solver;
import utility.Enums;

public class VarHManager {
    private final Solver solver;
    private HeuristicVariables h;
    public final Enums.EWeighting w;

    public VarHManager(Solver solver, HeuristicVariables h, Enums.EWeighting w) {
        this.solver = solver;
        this.h = h;
        this.w = w;
    }

    public void select() {
        this.solver.head.control.varh.weighting = w;
    }

    public HeuristicVariables getVarH() {
        return this.h;
    }

    @Override
    public String toString() {
        return h.getClass().getSimpleName() + (this.w == null ? "" : "-" + w);
    }

}
