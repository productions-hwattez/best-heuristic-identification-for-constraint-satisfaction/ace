package heuristics.composition;

import heuristics.HeuristicVariablesDirect;
import heuristics.HeuristicVariablesDynamic;
import heuristics.HeuristicVariablesFixed;
import solver.Solver;
import utility.Enums;

import java.util.function.Function;

public enum HeuristicBuilder {
    WDEG(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false), Enums.EWeighting.UNIT)),
    CACD(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false), Enums.EWeighting.CACD)),
    CACD_DEG(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 0), Enums.EWeighting.CACD)),
    CACD1(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 1), Enums.EWeighting.CACD)),
    CACD2(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 2), Enums.EWeighting.CACD)),
    CACD3(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 3), Enums.EWeighting.CACD)),
    CACD4(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 4), Enums.EWeighting.CACD)),
    CACD5(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Wdeg(solver, false, 5), Enums.EWeighting.CACD)),
    CHS(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false), Enums.EWeighting.CHS)),
    CHS_DEG(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 0), Enums.EWeighting.CHS)),
    CHS1(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 1), Enums.EWeighting.CHS)),
    CHS2(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 2), Enums.EWeighting.CHS)),
    CHS3(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 3), Enums.EWeighting.CHS)),
    CHS4(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 4), Enums.EWeighting.CHS)),
    CHS5(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, 5), Enums.EWeighting.CHS)),
    CHSa1(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, -1, .1), Enums.EWeighting.CHS)),
    CHSa3(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, -1, .3), Enums.EWeighting.CHS)),
    CHSa5(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, -1, .5), Enums.EWeighting.CHS)),
    CHSa7(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, -1, .7), Enums.EWeighting.CHS)),
    CHSa9(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.WdegOnDom(solver, false, -1, .9), Enums.EWeighting.CHS)),
    ACTIVITY(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false), null)),
    ACTIVITY_DEG(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 0), null)),
    ACTIVITY1(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 1), null)),
    ACTIVITY2(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 2), null)),
    ACTIVITY3(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 3), null)),
    ACTIVITY4(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 4), null)),
    ACTIVITY5(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Activity(solver, false, 5), null)),
    IMPACT(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false), null)),
    IMPACT_DEG(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 0), null)),
    IMPACT1(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 1), null)),
    IMPACT2(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 2), null)),
    IMPACT3(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 3), null)),
    IMPACT4(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 4), null)),
    IMPACT5(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Impact(solver, false, 5), null)),
    DDEGONDOM(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.DdegOnDom(solver, false), null)),
    DOM(solver -> new VarHManager(solver, new HeuristicVariablesDynamic.Dom(solver, false), null)),
    LEX(solver -> new VarHManager(solver, new HeuristicVariablesFixed.Lexico(solver, false), null)),
    RAND(solver -> new VarHManager(solver, new HeuristicVariablesDirect.Rand(solver, false), null));


    private Function<Solver, VarHManager> constructor;

    private HeuristicBuilder(Function<Solver, VarHManager> constructor) {
        this.constructor = constructor;
    }

    public VarHManager build(Solver solver) {
        return this.constructor.apply(solver);
    }

}
