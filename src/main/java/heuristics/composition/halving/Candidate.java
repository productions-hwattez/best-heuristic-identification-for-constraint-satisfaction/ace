package heuristics.composition.halving;

import heuristics.composition.VarHManager;

public class Candidate {

    public double score = -1;
    VarHManager varHManager;

    public Candidate(VarHManager varHManager) {
        this.varHManager = varHManager;
    }
}
