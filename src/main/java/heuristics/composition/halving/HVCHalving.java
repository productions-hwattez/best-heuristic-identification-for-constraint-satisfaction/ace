package heuristics.composition.halving;

import constraints.Constraint;
import heuristics.composition.HeuristicVariableComposition;
import heuristics.composition.VarHManager;
import heuristics.composition.tournament.MDuels;
import solver.Solver;
import utility.Reflector;
import variables.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HVCHalving extends HeuristicVariableComposition {

    private final List<Candidate[]> ashList = new ArrayList<>();
    private int level = 0;
    private int nCandidate = 0;


    public HVCHalving(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);
        this.prepapreLevel(0);
    }

    private void prepapreLevel(int level) {
        Candidate[] candidates = null;

        if (level >= this.ashList.size()){
            // New level
            int size = Math.max(2, this.varHManagers.size() >> level);
            candidates = new Candidate[size];
            this.ashList.add(candidates);
        } else {
            candidates = this.ashList.get(level);
        }

        if (level == 0) {
            // Level 0
            for (int i=0; i<candidates.length; i++) {
                candidates[i] = new Candidate(this.varHManagers.get(i));
            }

            Collections.shuffle(this.varHManagers);
        } else {
            // General case for the other levels
            Candidate[] previousCandidates = this.ashList.get(level - 1);
            Arrays.sort(previousCandidates, (a, b) -> -Double.compare(a.score, b.score));

            // Depends if the two last levels are both 2candidate-versus
            if (previousCandidates.length != candidates.length) {
                for (int i = 0; i < candidates.length; i++) {
                    candidates[i] = new Candidate(previousCandidates[i].varHManager);
                }
            } else {
                candidates[candidates[0] == null ? 0 : 1] = new Candidate(previousCandidates[0].varHManager);
            }

            Arrays.fill(previousCandidates, null);
        }
    }

    @Override
    public void internalBeforeRun() {
        this.selectNextVarH();
        this.currentVarH.beforeRun();

        // We force the cutoff updating
        this.solver.restarter.offset = 150 * (1 << this.level);
        this.solver.restarter.currCutoff = this.solver.restarter.beforeRunCutoff + this.solver.restarter.offset;
    }

    private void selectNextVarH() {
        this.currentVarHManager = this.ashList.get(level)[nCandidate].varHManager;
    }

    @Override
    public void afterRun() {
        this.reward.afterRun();
        this.computeTheCandidateScore();
        this.currentVarH.afterRun();
    }

    private void computeTheCandidateScore() {
        this.ashList.get(level)[nCandidate].score = this.reward.getReward();

        if (nCandidate < this.ashList.get(level).length - 1) {
            if (this.ashList.get(level)[nCandidate + 1] != null) {
                // If it remains candidates in the current level
                nCandidate++;
            } else {
                this.level = 0;
                this.prepapreLevel(0);
                this.nCandidate = 0;
            }
        } else {
            // Next level !
            this.level++;
            this.prepapreLevel(this.level);
            this.nCandidate = this.ashList.get(level)[0].score == -1 ? 0 : 1;
        }
    }


    @Override
    public void whenWipeout(Constraint c, Variable x) {
        this.currentVarH.whenWipeout(c, x);
    }

    @Override
    public void beforeAssignment(Variable x, int a) {
        this.currentVarH.beforeAssignment(x, a);
    }

    @Override
    public void afterAssignment(Variable x, int a) {
        this.currentVarH.afterAssignment(x, a);
    }

    @Override
    public void afterUnassignment(Variable x) {
        this.currentVarH.afterUnassignment(x);
    }

    @Override
    public double scoreOf(Variable x) {
        return this.currentVarH.scoreOf(x);
    }

    @Override
    public Variable bestUnpriorityVar() {
        return this.currentVarH.bestUnpriorityVar();
    }

    @Override
    public String toString() {
        return this.currentVarHManager.toString();
    }

}
