/**
 * AbsCon - Copyright (c) 2017, CRIL-CNRS - lecoutre@cril.fr
 * 
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL which accompanies this
 * distribution, and is available at http://www.cecill.info
 */
package heuristics;

import constraints.Constraint;
import solver.Solver;
import variables.Variable;

public abstract class HeuristicVariablesDirect extends HeuristicVariables {

	public HeuristicVariablesDirect(Solver solver, boolean antiHeuristic) {
		super(solver, antiHeuristic);
	}

	@Override
	public double scoreOf(Variable x) {
		throw new AssertionError("The variable must be directly selected without any iteration");
	}

	/*************************************************************************
	 * Subclasses
	 *************************************************************************/

	public static final class Rand extends HeuristicVariablesDirect {

		public Rand(Solver solver, boolean antiHeuristic) {
			super(solver, antiHeuristic);
		}

		@Override
        public Variable bestUnpriorityVar() {
			return solver.futVars.get(solver.head.random.nextInt(solver.futVars.size()));
		}

		@Override
		public void beforeAssignment(Variable x, int a) {

		}

		@Override
		public void afterAssignment(Variable x, int a) {

		}

		@Override
		public void afterUnassignment(Variable x) {

		}

		@Override
		public void whenWipeout(Constraint c, Variable x) {

		}
	}

}
