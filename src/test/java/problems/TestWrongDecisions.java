package problems;

import static org.junit.Assert.assertEquals;
import static problems.UtilityForTests.runResolution;

import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.xcsp.common.Utilities;

import main.Head;

@RunWith(Parameterized.class)
public class TestWrongDecisions {

	static Collection<Object[]> collection = new LinkedList<>();

	static void add(Object instance, int nWrongDecisions, String pars) {
		pars += " -ev";
		URL url = Head.class.getResource(instance + ".xml.lzma");
		Utilities.control(url != null, "not found: " + instance + ".xml.lzma");
		collection.add(new Object[] { url.getPath() + " " + pars, nWrongDecisions });
	}

	static void add(String instance, int nWrongDecisions) {
		add(instance, nWrongDecisions, "");
	}

	@Parameters(name = "{index}: {0} has {1} wrong decisions")
	public static Collection<Object[]> data() {

		// Halving multi-heuristics
		add("/csp/Rlfap-scen-11-f06", 10551, "-r_luby=True -varh=HVCHalving -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT,WDEG,LEX,DOM -reward=NPTSReward -bandit=NullPolicy -r_rrp=99999999");

		// 8 heuristics
		add("/csp/Rlfap-scen-11-f06", 32226, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT,WDEG,LEX,DOM -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999");
		add("/csp/Rlfap-scen-11-f06", 28839, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT,WDEG,LEX,DOM -reward=NPTSReward -bandit=Ucb");
		add("/csp/Rlfap-scen-11-f06", 14297, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT,WDEG,LEX,DOM -reward=NPTSReward -bandit=Exp3");
		add("/csp/Crossword-lex-vg-5-6", 13073, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT,WDEG,LEX,DOM -reward=NullReward -bandit=UniformPolicy");

		// Perturbed heuristics by ST bandit
		add("/csp/Rlfap-scen-11-f06", 19961, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=CHS,RAND -reward=AUVRReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=2 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 22017, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=CHS,RAND -reward=NPTSReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=4 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 26482, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=CHS,RAND -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=8 -decSubRuns=true");
		add("/csp/Crossword-lex-vg-5-6", 103308, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=ACTIVITY,RAND -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=16 -decSubRuns=true");
		add("/csp/Crossword-lex-vg-5-6", 79795, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=IMPACT,RAND -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=1 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 164505, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=DDEGONDOM,RAND -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=2 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 22915, "-r_luby=True -varh=PerturbedHVCSingleTournament -varhSet=CACD,RAND -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=4 -decSubRuns=true");

		// Perturbation by many seeds
		add("/csp/Rlfap-scen-11-f06", 17534, "-varh=HVCAtRun -varhSet=CACD,CACD_DEG,CACD3,CACD4,CACD5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 15324, "-varh=HVCAtRun -varhSet=CHS,CHS_DEG,CHS3,CHS4,CHS5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 11759, "-varh=HVCAtRun -varhSet=ACTIVITY,ACTIVITY_DEG,ACTIVITY3,ACTIVITY4,ACTIVITY5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 63846, "-varh=HVCAtRun -varhSet=IMPACT,IMPACT_DEG,IMPACT3,IMPACT4,IMPACT5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 14157, "-varh=HVCAtRun -varhSet=CACD1,CACD2,CACD3,CACD4,CACD5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 15324, "-varh=HVCAtRun -varhSet=CHS1,CHS2,CHS3,CHS4,CHS5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 14254, "-varh=HVCAtRun -varhSet=CHSa1,CHSa3,CHSa5,CHSa7,CHSa9 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 7573, "-varh=HVCAtRun -varhSet=ACTIVITY1,ACTIVITY2,ACTIVITY3,ACTIVITY4,ACTIVITY5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 141297, "-varh=HVCAtRun -varhSet=IMPACT1,IMPACT2,IMPACT3,IMPACT4,IMPACT5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 17972, "-varh=HVCAtRun -varhSet=CACD1,CACD2,CACD3,CACD4,CACD5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 15467, "-varh=HVCAtRun -varhSet=CHS1,CHS2,CHS3,CHS4,CHS5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 15406, "-varh=HVCAtRun -varhSet=CHSa1,CHSa3,CHSa5,CHSa7,CHSa9 -reward=ESBReward -bandit=Exp3");
		add("/csp/Crossword-lex-vg-5-6", 12066, "-varh=HVCAtRun -varhSet=ACTIVITY1,ACTIVITY2,ACTIVITY3,ACTIVITY4,ACTIVITY5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Crossword-lex-vg-5-6", 6628, "-varh=HVCAtRun -varhSet=IMPACT1,IMPACT2,IMPACT3,IMPACT4,IMPACT5 -reward=ESBReward -bandit=Exp3");

		// Championship with multi decreasing sub-runs
		add("/csp/Rlfap-scen-11-f06", 21531, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=2 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 27823, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=4 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 22674, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=8 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 32510, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=16 -decSubRuns=true");
		add("/csp/Rlfap-scen-11-f06", 14278, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=32 -decSubRuns=true");

		// Championship with multi sub-runs
		add("/csp/Rlfap-scen-11-f06", 14957, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=33 -defMDuels=MDuelsMax");
		add("/csp/Rlfap-scen-11-f06", 14753, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=33 -defMDuels=MDuelsMean");
		add("/csp/Rlfap-scen-11-f06", 14753, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=33");
		add("/csp/Rlfap-scen-11-f06", 14440, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=17");
		add("/csp/Rlfap-scen-11-f06", 23348, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=16");
		add("/csp/Rlfap-scen-11-f06", 14278, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=32");
		add("/csp/Rlfap-scen-11-f06", 25654, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999");
		add("/csp/Rlfap-scen-11-f06", 16183, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD1,CACD2,CACD3,CACD4,CACD5 -reward=NPTSReward -bandit=NullPolicy -r_rrp=99999999");
		add("/csp/Rlfap-scen-11-f06", 18970, "-r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CACD_DEG,CACD1,CACD2,CACD3 -reward=NPTSReward -bandit=NullPolicy -r_rrp=99999999");
		add("/csp/Rlfap-scen-11-f06", 20237, "-r_luby=True -varh=HVCSingleTournament -varhSet=CHSa1,CHSa3,CHSa5,CHSa7,CHSa9 -reward=AUVRReward -bandit=NullPolicy -r_rrp=99999999");

		// Xia and Yap
		add("/csp/Crossword-lex-vg-5-6", 27665, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 17369, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=Moss");
		add("/csp/Rlfap-scen-11-f06", 143687, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=Ucb");
		add("/csp/Crossword-lex-vg-5-6", 65667, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=TS");
		add("/csp/Rlfap-scen-11-f06", 26341, "-varh=HVCAtNode -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=UniformPolicy");

		// Problematic instances
		add("/csp/Haystacks-ext-07_c18", 10710816, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=NullReward -bandit=BiasedPolicy -biasedDistrib=.7,.3");
		add("/csp/Blackhole-13-3-01", 0, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=EpsilonGreedy");

		// Perturbation by many seeds
		add("/csp/Rlfap-scen-11-f06", 14157, "-varh=HVCAtRun -varhSet=CACD1,CACD2,CACD3,CACD4,CACD5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 15324, "-varh=HVCAtRun -varhSet=CHS1,CHS2,CHS3,CHS4,CHS5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 14254, "-varh=HVCAtRun -varhSet=CHSa1,CHSa3,CHSa5,CHSa7,CHSa9 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 7573, "-varh=HVCAtRun -varhSet=ACTIVITY1,ACTIVITY2,ACTIVITY3,ACTIVITY4,ACTIVITY5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 141297, "-varh=HVCAtRun -varhSet=IMPACT1,IMPACT2,IMPACT3,IMPACT4,IMPACT5 -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 17972, "-varh=HVCAtRun -varhSet=CACD1,CACD2,CACD3,CACD4,CACD5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 15467, "-varh=HVCAtRun -varhSet=CHS1,CHS2,CHS3,CHS4,CHS5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 15406, "-varh=HVCAtRun -varhSet=CHSa1,CHSa3,CHSa5,CHSa7,CHSa9 -reward=ESBReward -bandit=Exp3");
		add("/csp/Crossword-lex-vg-5-6", 12066, "-varh=HVCAtRun -varhSet=ACTIVITY1,ACTIVITY2,ACTIVITY3,ACTIVITY4,ACTIVITY5 -reward=ESBReward -bandit=Exp3");
		add("/csp/Crossword-lex-vg-5-6", 6628, "-varh=HVCAtRun -varhSet=IMPACT1,IMPACT2,IMPACT3,IMPACT4,IMPACT5 -reward=ESBReward -bandit=Exp3");

		// Gomes equivalence parameter
		add("/csp/Rlfap-scen-11-f06", 13309, "-varh=Wdeg -wt=CACD -varEqui=.3");
		add("/csp/Rlfap-scen-11-f06", 14747, "-varh=WdegOnDom -wt=CHS -varEqui=.3");
		add("/csp/Rlfap-scen-11-f06", 46928, "-varh=DdegOnDom -varEqui=.3");
		add("/csp/Crossword-lex-vg-5-6", 122011, "-varh=Impact -varEqui=.3");
		add("/csp/Crossword-lex-vg-5-6", 128221, "-varh=Activity -varEqui=.3");
		add("/csp/Rlfap-scen-11-f06", 15295, "-varh=WdegOnDom -varEqui=1e-10");

		// Grimes perturbation
		add("/csp/Rlfap-scen-11-f06", 54082, "-varh=GrimesSamplingHVCAtRun -varhSet=CHS,RAND -reward=NullReward -bandit=NullPolicy");
		add("/csp/Rlfap-scen-11-f06", 51493, "-varh=GrimesSamplingHVCAtRun -varhSet=CACD,RAND -reward=NullReward -bandit=NullPolicy");
		add("/csp/Crossword-lex-vg-5-6", 83349, "-varh=GrimesSamplingHVCAtRun -varhSet=ACTIVITY,RAND -reward=NullReward -bandit=NullPolicy");
		add("/csp/Crossword-lex-vg-5-6", 10078, "-varh=GrimesSamplingHVCAtRun -varhSet=IMPACT,RAND -reward=NullReward -bandit=NullPolicy");
		add("/csp/Rlfap-scen-11-f06", 89099, "-varh=GrimesSamplingHVCAtRun -varhSet=DDEGONDOM,RAND -reward=NullReward -bandit=NullPolicy");

		// Perturbed heuristics by fixed distrib
		add("/csp/Rlfap-scen-11-f06", 17983, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=NullReward -bandit=BiasedPolicy -biasedDistrib=.9,.1");
		add("/csp/Rlfap-scen-11-f06", 75080, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=NullReward -bandit=UniformPolicy");

		// Perturbed heuristics by bandits
		add("/csp/Rlfap-scen-11-f06", 20789, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=AUVRReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 19106, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=NPTSReward -bandit=EpsilonGreedy");

		add("/csp/Rlfap-scen-11-f06", 20789, "-varh=PerturbedHVCAtRun -varhSet=CHS,RAND -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 7979, "-varh=PerturbedHVCAtRun -varhSet=ACTIVITY,RAND -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Crossword-lex-vg-5-6", 78464, "-varh=PerturbedHVCAtRun -varhSet=IMPACT,RAND -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 49753, "-varh=PerturbedHVCAtRun -varhSet=DDEGONDOM,RAND -reward=ESBReward -bandit=EpsilonGreedy");

		add("/csp/Rlfap-scen-11-f06", 19152, "-varh=PerturbedHVCAtRun -varhSet=CACD,RAND -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 24997, "-varh=PerturbedHVCAtRun -varhSet=CACD,RAND -reward=ESBReward -bandit=Ucb");
		add("/csp/Rlfap-scen-11-f06", 32021, "-varh=PerturbedHVCAtRun -varhSet=CACD,RAND -reward=ESBReward -bandit=Moss");
		add("/csp/Rlfap-scen-11-f06", 12362, "-varh=PerturbedHVCAtRun -varhSet=CACD,RAND -reward=ESBReward -bandit=TS");
		add("/csp/Rlfap-scen-11-f06", 35568, "-varh=PerturbedHVCAtRun -varhSet=CACD,RAND -reward=ESBReward -bandit=Exp3");

		// Uniform
		add("/csp/Rlfap-scen-11-f06", 36924, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NullReward -bandit=UniformPolicy");

		// AUVR & classical bandits
		add("/csp/Rlfap-scen-11-f06", 40843, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 32876, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=Ucb");
		add("/csp/Rlfap-scen-11-f06", 27272, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=Moss");
		add("/csp/Crossword-lex-vg-5-6", 11223, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 26143, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=AUVRReward -bandit=TS");

		// ESB & classical bandits
		add("/csp/Rlfap-scen-11-f06", 40843, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 28249, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=Ucb");
		add("/csp/Rlfap-scen-11-f06", 14700, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=Moss");
		add("/csp/Rlfap-scen-11-f06", 37470, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 41135, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=TS");

		// NPTS & classical bandits
		add("/csp/Rlfap-scen-11-f06", 40843, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=EpsilonGreedy");
		add("/csp/Rlfap-scen-11-f06", 25357, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=Ucb");
		add("/csp/Rlfap-scen-11-f06", 45984, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=Moss");
		add("/csp/Crossword-lex-vg-5-6", 59751, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=Exp3");
		add("/csp/Rlfap-scen-11-f06", 30303, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=TS");

		// Classical heuristics and tie breakers
		add("/csp/Rlfap-scen-11-f06", 16383, "-varh=WdegOri");
		add("/csp/Rlfap-scen-11-f06", 13295, "-varh=WdegOnDomOri");
		add("/csp/Rlfap-scen-11-f06", 10974, "-varh=Wdeg -varhbias=RAND");
		add("/csp/Rlfap-scen-11-f06", 14407, "-varh=Wdeg -varhbias=DEG");
		add("/csp/Rlfap-scen-11-f06", 10959, "-varh=Wdeg -varhbias=LEX");
		add("/csp/Rlfap-scen-11-f06", 12639, "-varh=WdegOnDom -varhbias=DEG");
		add("/csp/Rlfap-scen-11-f06", 50748, "-varh=DdegOnDom -varhbias=RAND");
		add("/csp/Crossword-lex-vg-5-6", 559, "-varh=Impact -varhbias=RAND");
		add("/csp/Crossword-lex-vg-5-6", 6969, "-varh=Activity -varhbias=RAND");
		add("/csp/Rlfap-scen-11-f06", 16383, "-varh=Wdeg -wt=CACD -cacd=CA");
		add("/csp/Rlfap-scen-11-f06", 12459, "-varh=Wdeg -wt=CACD -cacd=CD");
		add("/csp/Rlfap-scen-11-f06", 16383, "-varh=Wdeg -wt=CACD -cacd=IA");
		add("/csp/Rlfap-scen-11-f06", 10526, "-varh=Wdeg -wt=CACD -cacd=ID");
		add("/csp/Rlfap-scen-11-f06", 10959, "-varh=Wdeg -wt=CACD");
		add("/csp/Rlfap-scen-11-f06", 13024, "-varh=WdegOnDom -wt=CHS");
		add("/csp/Rlfap-scen-11-f06", 13112, "-varh=WdegOnDom -wt=UNIT");
		add("/csp/Rlfap-scen-11-f06", 50748, "-varh=DdegOnDom");
		add("/csp/Crossword-lex-vg-5-6", 23923, "-varh=Impact");
		add("/csp/Crossword-lex-vg-5-6", 36677, "-varh=Activity");
		add("/csp/Rlfap-scen-11-f06", 24277, "-varh=Dom");
		add("/csp/Crossword-lex-vg-5-6", 1636, "-varh=Lexico");

		// Originally here
		add("/csp/Rlfap-scen-11-f06", 13121, "-varh=WdegOnDom");
		add("/csp/Crossword-lex-vg-5-6", 2426, "-varh=WdegOnDom");
		add("/csp/Crossword-lex-vg-5-6", 9199, "-varh=DdegOnDom -positive=str1");
		add("/csp/Crossword-lex-vg-5-6", 9199, "-varh=DdegOnDom -positive=str2");
		add("/csp/Crossword-lex-vg-5-6", 9199, "-varh=DdegOnDom -positive=str3");
		add("/csp/Crossword-lex-vg-5-6", 9199, "-varh=DdegOnDom -positive=cmdd");

		return collection;
	}

	@Parameter(0)
	public String args;

	@Parameter(1)
	public int nWrongDecisions;

	@Test
	public void test() throws InterruptedException {
		assertEquals(nWrongDecisions, runResolution(args).solver.stats.nWrongDecisions);
	}
}
